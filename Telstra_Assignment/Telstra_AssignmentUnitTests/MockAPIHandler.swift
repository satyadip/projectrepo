//
//  MockAPIHandler.swift
//  Telstra_AssignmentUnitTests
//
//  Created by wipro on 9/5/18.
//  Copyright © 2018 wipro. All rights reserved.
//

import UIKit
@testable import Telstra_Assignment

class MockAPIHandler: APIHandlerProtocol {
    func callService(urlString: String, complitionHandler: @escaping ((Dictionary<String, Any>) -> Void)) {
        let testBundle = Bundle(for: type(of: self))
        if let filePath = testBundle.path(forResource: "JsonData", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: filePath), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                if let jsonResult = jsonResult as? Dictionary<String, AnyObject> {
                    print(jsonResult)
                    complitionHandler(jsonResult)
                }
            } catch {
                print("Error")
            }
        }
    }
}
