//
//  Telstra_AssignmentUnitTests.swift
//  Telstra_AssignmentUnitTests
//
//  Created by wipro on 05/09/18.
//  Copyright © 2018 wipro. All rights reserved.
//

import XCTest
@testable import Telstra_Assignment

class Telstra_AssignmentUnitTests: XCTestCase {
    
    var objVC: ViewController! = nil
    
    var arrItems : NSMutableArray = []
    
    override func setUp() {
        super.setUp()
        
        objVC = ViewController()
        _ = objVC.view
    }
    
    //MARK:- Test case methods
    
    func testInitTableView() {
        XCTAssertNotNil(objVC.tblView)
    }
    
    func testTableViewdataSource() {
        XCTAssertTrue(objVC.tblView.dataSource is ViewController)
    }
    
    func testTableViewDelegate() {
        XCTAssertTrue(objVC.tblView.delegate is ViewController)
    }
    
    func testDataSourceDelegateSameInstance() {
        XCTAssertEqual(objVC.tblView.dataSource as! ViewController, objVC.tblView.delegate as! ViewController)
    }
    
    func testNetworkAvailable() {
        objVC.networkReachable()
        XCTAssertEqual(objVC.isNetworkAvailable, true)
    }
    
    func testJsonArrayCount() {
        objVC.callHandler = MockAPIHandler()
        objVC.isNetworkAvailable = true
        objVC.serviceCall(urlString: "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json")
        XCTAssertEqual(objVC.arrItems.count, 2)
    }
    
    func testCheckTitleFromJSON() {
        XCTAssertNotNil(objVC.lblTitle.text)
    }
    
    /**
     Method to check positive test case. It should pass as URL is correct.
     
     */
    
    func testCount() {
        
        objVC.isNetworkAvailable = true
        let expectation = XCTestExpectation(description: "Checking JSON data and table element count same or not")
        let urlString = "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json"
        objVC.testServiceCall(strURL: urlString) { (result) in
            expectation.fulfill()
            XCTAssertEqual(result.count, 14)
        }
        wait(for: [expectation], timeout: 10.0)
    }
    
    /**
     Method to check negetive test case. Given wrong URL
     
     */
    
    func testJSONResponse() {
        
        objVC.isNetworkAvailable = true
        let expectation = XCTestExpectation(description: "Checking data is not giving response for wrong URL")
        let strURL = "https://dl.dropboxuser"
        objVC.testServiceCall(strURL: strURL) { (response) in
            expectation.fulfill()
            XCTAssertEqual(response.count, 10)
        }
        wait(for: [expectation], timeout: 10.0)
    }
    
    /**
     Method to check title matching with JSON Data
     
     */
    
    
    func testTitleHeader() {
        objVC.isNetworkAvailable = true
        let expectation = XCTestExpectation(description: "Checking data is not giving response for wrong URL")
        let urlString = "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json"
        objVC.testServiceCall(strURL: urlString) { (value) in
            expectation.fulfill()
            XCTAssertEqual(self.objVC.strTitle, "About Canada")
        }
        wait(for: [expectation], timeout: 10.0)
    }
}
