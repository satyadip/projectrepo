//
//  ViewController.swift
//  Telstra_Assignment
//
//  Created by wipro on 28/08/18.
//  Copyright © 2018 wipro. All rights reserved.
//

import UIKit


class ViewController: UIViewController {

    var arrItems : [[String:Any]] = []
    var strTitle : String?
    let networkManager = NetworkManager()
    var callHandler : APIHandlerProtocol?
    var isNetworkAvailable : Bool = false
    var JSONData :  [String : Any] = [:]
    
    var tblView: UITableView!
    let cellReuseIdentifier = "cell"
    var contentTitle: UIView = UIView()
    var lblTitle: UILabel = UILabel()
    var titleJSON : String?
    var topViewBottomConstraint : Float = 20.0
    var urlString : String = "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json"
    
  
    // refresh controller declaration
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(ViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.black
        refreshControl.backgroundColor = UIColor.clear
        return refreshControl
    }()
    
    //MARK:- Self Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let notificationCenter = NotificationCenter.default

        notificationCenter.addObserver(self,
                                       selector: #selector(networkUnReachable),
                                       name: NetworkUnreachableNotification,
                                       object: nil)
        notificationCenter.addObserver(self,
                                       selector: #selector(networkReachable),
                                       name: NetworkReachableNotification,
                                       object: nil)
        
        callHandler = APIHandler() as APIHandlerProtocol
        setUpHeader()
        setUpTableView()
        showBlurLoader()

        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- Private Methods
    
    /**
     Method to call URL and update data
     
     - warning : No
     - parameter No : No
     - returns : No
     
     */
    
    func serviceCall(urlString : String) {
        
        guard isNetworkAvailable else {
            self.view.makeToast(message: "Network is not available", duration: 3, position: HRToastPositionDefault as AnyObject)
            return
        }
        
        callHandler?.callService(urlString: urlString,complitionHandler: { result in
            
            if result.count > 0 {
                weak var wVC = self
                wVC!.JSONData = result
                wVC?.updateJSONData()

            }
            else {
                self.view.makeToast(message: "Something went wrong", duration: 3, position: HRToastPositionDefault as AnyObject)
                return
            }
            
        })
        
    }
    
    /**
     Method to setup header view
     
     - warning : No
     - parameter No : No
     - returns : No
     
     */
    
    func setUpHeader() {
        
        let displayWidth: CGFloat = view.bounds.size.width
        let displayHeight: CGFloat = view.bounds.size.height
        
        if UIDevice.current.orientation.isLandscape {
            contentTitle.frame = CGRect(x: 0, y: 0, width: max(displayWidth, displayHeight), height: 60)
        } else {
            contentTitle.frame = CGRect(x: 0, y: 0, width: min(displayWidth, displayHeight), height: 60)
        }
        
        if UI_USER_INTERFACE_IDIOM() == .phone {
            let screenSize: CGSize = UIScreen.main.bounds.size
            if screenSize.height == 812.0 {
                topViewBottomConstraint = 30.0
            }
        }
        
        contentTitle.backgroundColor = UIColor.green
        
        self.view.addSubview(contentTitle)
        self.contentTitle.addSubview(lblTitle)
        
        lblTitle.leftAnchor.constraint(equalTo: self.contentTitle.leftAnchor).isActive = true
        lblTitle.rightAnchor.constraint(equalTo: self.contentTitle.rightAnchor).isActive = true
        lblTitle.topAnchor.constraint(equalTo: self.contentTitle.topAnchor,constant: CGFloat(topViewBottomConstraint)).isActive = true
        
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        lblTitle.heightAnchor.constraint(equalToConstant: 21).isActive = true
        
        lblTitle.textColor = UIColor.black
        lblTitle.font = UIFont(name:"HelveticaNeue-Bold", size: 20.0)
        lblTitle.textAlignment = NSTextAlignment.center
        lblTitle.text = "Title from JSON"
    }
    
    /**
     Method to setup table view
     
     - warning : No
     - parameter No : No
     - returns : No
     
     */
    
    func setUpTableView() {
        let barHeight: CGFloat = 60.0
        let displayWidth: CGFloat = view.bounds.size.width
        let displayHeight: CGFloat = view.bounds.size.height
        
        if UIDevice.current.orientation.isLandscape {
            tblView = UITableView(frame: CGRect(x: 0, y: barHeight, width: max(displayWidth, displayHeight), height: min(displayWidth, displayHeight) - barHeight), style: .plain)
        } else {
            tblView = UITableView(frame: CGRect(x: 0, y: barHeight, width: min(displayWidth, displayHeight), height: max(displayWidth, displayHeight) - barHeight), style: .plain)
        }

        tblView.register(CustomCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        tblView.dataSource = self
        tblView.delegate = self
        
        tblView.addSubview(self.refreshControl)
        
        self.view.addSubview(tblView)
    }
    
    /**
     Method to handle refresh controller
     
     - warning : No
     - parameter refreshControl : UIRefreshControl
     - returns : No
     
     */
    
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        serviceCall(urlString: urlString)
        self.refreshControl.endRefreshing()
    }
    
    /**
     Method get calls when network is Unreachable
     
     - warning : No
     - parameter No : No
     - returns : No
     
     */
    
    
    @objc func networkUnReachable() {
        isNetworkAvailable = false
        self.view.makeToast(message: "Network is not available", duration: 3, position: HRToastPositionDefault as AnyObject)
    }
    
    /**
     Method get calls when network is reachable
     
     - warning : No
     - parameter No : No
     - returns : No
     
     */
    
    @objc func networkReachable() {
        
        isNetworkAvailable = true
        serviceCall(urlString: urlString)
    }
    
    /**
     Method to show view is blur
     
     - warning : No
     - parameter No : No
     - returns : No
     
     */
    
    
    func showBlurLoader(){
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityIndicator.startAnimating()
        
        blurEffectView.contentView.addSubview(activityIndicator)
        activityIndicator.center = blurEffectView.contentView.center
        
        self.view.addSubview(blurEffectView)
    }
    
    /**
     Method to hide blur view
     
     - warning : No
     - parameter No : No
     - returns : No
     
     */
    
    func removeBluerLoader(){
        self.view.subviews.flatMap {  $0 as? UIVisualEffectView }.forEach {
            $0.removeFromSuperview()
        }
    }
    
    /**
     Method get calls when device orientation changes
     */
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        setUpHeader()
        setUpTableView()
    }
    
    /**
     Method to update JSON data in UI elements
     */
    
    
    func updateJSONData() {
        if let array = JSONData["rows"] as? [[String:Any]] {
            self.arrItems = array
        }
        DispatchQueue.main.async {
            if let title = self.JSONData["title"] as? String {
                
                self.titleJSON = title
                self.lblTitle.text = title
                self.tblView.reloadData()
                self.removeBluerLoader()
            }
        }
    }
    
    /**
     Method which return JSON data on completion of URL request
     */
    
    func testServiceCall(strURL : String, complitionHandler: @escaping (([[String:Any]])->Void)) {
        
        callHandler?.callService(urlString: strURL,complitionHandler: { result in
            
            if result.count > 0 {
                
                weak var wVC = self
                wVC!.JSONData = result
                wVC?.updateJSONData()
                let array = result["rows"] as? [[String:Any]]
                if let title = self.JSONData["title"] as? String {
                    self.strTitle = title
                }
                complitionHandler(array!)
                
            }
            else {
                let arrElement = [[String:Any]]()
                complitionHandler(arrElement)
            }
            
        })
        
    }
    
    
}

//MARK:- Delegate and DataSource

extension ViewController : UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrItems.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       let cell : CustomCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! CustomCell
        
       let items = arrItems[indexPath.row]
       cell.bindCellData(data: items)
       cell.selectionStyle = .none

       cell.layoutIfNeeded()
       return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        tblView.estimatedRowHeight = 100
        return UITableViewAutomaticDimension
    }

}


