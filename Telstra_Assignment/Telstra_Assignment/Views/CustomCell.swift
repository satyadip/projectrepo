//
//  CustomCell.swift
//  Telstra_Assignment
//
//  Created by wipro on 04/09/18.
//  Copyright © 2018 wipro. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell {
        
    var task: URLSessionDownloadTask!
    var session: URLSession!
    var cache:NSCache<AnyObject, AnyObject>!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    var messageTitle : UILabel = {
      var messageTitle = UILabel()
        messageTitle.font = UIFont(name:"HelveticaNeue-Bold", size: 16.0)
        messageTitle.translatesAutoresizingMaskIntoConstraints = false
        return messageTitle
    } ()
    
    var mainImageView : UIImageView = {
        var imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage.init(named: "placeholder")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    var messageDescription : UILabel = {
        var messageDescription = UILabel()
        messageDescription.font = UIFont(name:"HelveticaNeue", size: 13.0)
        messageDescription.translatesAutoresizingMaskIntoConstraints = false
        return messageDescription
    } ()
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        session = URLSession.shared
        task = URLSessionDownloadTask()
        self.cache = NSCache()
        
        self.addSubview(mainImageView)
        self.addSubview(messageTitle)
        self.addSubview(messageDescription)
        
        
        self.heightAnchor.constraint(greaterThanOrEqualToConstant: 100.0).isActive = true
        
        mainImageView.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 10).isActive = true
        mainImageView.widthAnchor.constraint(equalToConstant: 80).isActive = true
        mainImageView.topAnchor.constraint(equalTo: self.topAnchor,constant: 10).isActive = true
        mainImageView.heightAnchor.constraint(equalToConstant: 80).isActive = true

        messageTitle.leftAnchor.constraint(equalTo: self.mainImageView.rightAnchor,constant: 5).isActive = true
        messageTitle.rightAnchor.constraint(equalTo: self.rightAnchor,constant: 5).isActive = true
        messageTitle.topAnchor.constraint(equalTo: self.topAnchor,constant: 5).isActive = true
        messageTitle.heightAnchor.constraint(equalToConstant: 20).isActive = true
        messageTitle.numberOfLines = 0

        messageDescription.leftAnchor.constraint(equalTo: self.mainImageView.rightAnchor, constant: 5).isActive = true
        messageDescription.rightAnchor.constraint(equalTo: self.rightAnchor,constant: 0).isActive = true
        messageDescription.topAnchor.constraint(equalTo: self.messageTitle.bottomAnchor, constant: 5).isActive = true
        messageDescription.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        messageDescription.numberOfLines = 0
        
    }

    /**
     Method to display content in table view cell
     */
    
    
    func bindCellData(data : [String: Any]) {
        messageTitle.text = data["title"] as? String
        messageDescription.text = data["description"] as? String

        if let value = (data["imageHref"] as? String) {
            mainImageView.image = self.checkForImageToDownload(imageUrl: value)
        }else {
            mainImageView.image = UIImage.init(named: "placeholder")
        }
    }
    
    /**
     Method to download image if it is not downloaded
     */
    
    func checkForImageToDownload(imageUrl : String) -> UIImage? {
        if (self.cache.object(forKey: imageUrl as AnyObject) != nil){
            return self.cache.object(forKey: imageUrl as AnyObject) as? UIImage
        }else{
            let url:URL! = URL(string: imageUrl)
            task = session.downloadTask(with: url, completionHandler: { (location, response, error) -> Void in
                if let data = try? Data(contentsOf: url){
                    DispatchQueue.main.async(execute: { () -> Void in
                        if let img = UIImage(data: data) {
                            self.cache.setObject(img, forKey: imageUrl as AnyObject)
                            self.mainImageView.image = img
                        }else {
                            self.mainImageView.image = UIImage.init(named: "placeholder")
                        }
                    })
                }
            })
            task.resume()
        }
        return nil
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
