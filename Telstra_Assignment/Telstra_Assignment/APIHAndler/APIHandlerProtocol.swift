//
//  APIHandlerProtocol.swift
//  Telstra_Assignment
//
//  Created by wipro on 9/5/18.
//  Copyright © 2018 wipro. All rights reserved.
//

import UIKit

protocol APIHandlerProtocol {
    func callService(urlString: String, complitionHandler: @escaping ((Dictionary<String, Any>)->Void))
}

