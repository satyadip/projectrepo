//
//  APIHandler.swift
//  Telstra_Assignment
//
//  Created by wipro on 30/08/18.
//  Copyright © 2018 wipro. All rights reserved.
//


import UIKit

class APIHandler: APIHandlerProtocol {
    
    func callService(urlString: String, complitionHandler: @escaping ((Dictionary<String, Any>)->Void))  {
        
        let url = URL(string: urlString)
        
        let errDict = [:] as? Dictionary<String, AnyObject>
        
        URLSession.shared.dataTask(with: url!, completionHandler: {
            (data, response, error) in
            if(error != nil){
                print("error")
                complitionHandler(errDict!)
            }else{
                do{
                    let responseStrInISOLatin = String(data: data!, encoding: String.Encoding.isoLatin1)
                    let modifiedDataInUTF8Format = responseStrInISOLatin?.data(using: String.Encoding.utf8)
                    let responseJSONDict = try JSONSerialization.jsonObject(with: modifiedDataInUTF8Format!)
                    complitionHandler((responseJSONDict as? Dictionary<String, Any>)!)
                }catch let error as NSError{
                    print(error)
                    complitionHandler(errDict!)
                }
            }
        }).resume()

    }
}
