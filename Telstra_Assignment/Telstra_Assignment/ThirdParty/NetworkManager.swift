//
//  WRTCNetworkType.swift
//  WebRTCPlugIn
//
//  Created by Prakash on 23/01/18.
//  Copyright © 2018 Wipro. All rights reserved.
//

import Foundation
import CoreTelephony
import SystemConfiguration

let NetworkReachableNotification  = NSNotification.Name(rawValue: "networkReachable")
let NetworkUnreachableNotification  = NSNotification.Name(rawValue: "networkUnreachable")

public enum CurrentNetworkStatus {
    case CurrentNetworkTypeWiFi
    case CurrentNetworkType4G
    case CurrentNetworkType3G
    case CurrentNetworkType2G
    case CurrentNetworkTypeOthers
    case CurrentNetworkTypeNoInternet
}

public class NetworkManager:NSObject {
    
    public static let defaultManager = NetworkManager()
    
    var isNetworkAvailable : Bool {
        return reachabilityStatus != .none
    }
    var reachabilityStatus: Reachability.Connection = .none
    let reachability = Reachability()

    
    override init() {
        super.init()
        startMonitoring()
    }
    
    
    //MARK: Exposed methods
    /**
     Function to get current network status
     */
    public func getNetworkStatus() -> CurrentNetworkStatus {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return .CurrentNetworkTypeNoInternet
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return .CurrentNetworkTypeNoInternet
        }
        
        if flags.contains(.reachable) == false {
            // The target host is not reachable.
            return .CurrentNetworkTypeNoInternet
        }
        else if flags.contains(.isWWAN) == true {
            // WWAN connections are OK if the calling application is using the CFNetwork APIs.
            return NetworkManager.defaultManager.startCellularNetworkNotifier()
        }
        else if flags.contains(.connectionRequired) == false {
            // If the target host is reachable and no connection is required then we'll assume that you're on Wi-Fi...
            return .CurrentNetworkTypeWiFi
        }
        else if (flags.contains(.connectionOnDemand) == true || flags.contains(.connectionOnTraffic) == true) && flags.contains(.interventionRequired) == false {
            // The connection is on-demand (or on-traffic) if the calling application is using the CFSocketStream or higher APIs and no [user] intervention is needed
            return .CurrentNetworkTypeWiFi
        }
        else {
            return .CurrentNetworkTypeNoInternet
        }
        
        //print(currentNetworkStatus)
        
    }
    
    
    /**
     Function to get notified on network change
     */
    public func startCellularNetworkNotifier() -> CurrentNetworkStatus {
        
        switch CTTelephonyNetworkInfo().currentRadioAccessTechnology {
            
        case CTRadioAccessTechnologyGPRS?,CTRadioAccessTechnologyEdge?,CTRadioAccessTechnologyCDMA1x?:
            return .CurrentNetworkType2G
            
        case CTRadioAccessTechnologyWCDMA?, CTRadioAccessTechnologyHSDPA?, CTRadioAccessTechnologyHSUPA?,  CTRadioAccessTechnologyCDMAEVDORev0?, CTRadioAccessTechnologyCDMAEVDORevA?, CTRadioAccessTechnologyCDMAEVDORevB?, CTRadioAccessTechnologyeHRPD?:
            return .CurrentNetworkType3G
            
        case CTRadioAccessTechnologyLTE?:
            return .CurrentNetworkType4G
            
        default:
            return .CurrentNetworkTypeOthers
            
        }
        
    }
    
    /**
     Function to return maxBandwidth depending on network type
     - Parameter currentNetworkStat: current network status
     - return : maxBandwidth and accessType
     */
    public func getMakeCallHeader(_ currentNetworkStat:CurrentNetworkStatus) -> [AnyHashable : Any] {
        
        
        switch currentNetworkStat {
            
        case .CurrentNetworkType4G:

            return ["accessType":"4G"]
            
        case .CurrentNetworkType3G:

            return ["accessType":"3G"]
            
        case .CurrentNetworkType2G:
            return ["accessType":"2G"]
            
        case .CurrentNetworkTypeWiFi:

            return ["accessType":"WiFi"]
            
        case .CurrentNetworkTypeNoInternet:
            return ["accessType":"NoInternet"]
            
        case .CurrentNetworkTypeOthers:
            return ["accessType":"Unknown"]
            
        }
        
    }
    
    // MARK: Private methods
    /**
     Function to notify reachability change
     - Parameter notification: contains network info
     */
    @objc func reachabilityChanged(notification: Notification) {
        let reachability = notification.object as! Reachability
        switch reachability.connection {
        case .none:
            //debugPrint("Network became unreachable")
            NotificationCenter.default.post(name: NetworkUnreachableNotification, object: nil)
        case .wifi,.cellular:
            //debugPrint("Network became reachable")
            NotificationCenter.default.post(name: NetworkReachableNotification, object: nil)
        }
    }
    
    /**
     Function to set observer for reachability change
     */
    func startMonitoring() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.reachabilityChanged),
                                               name: Notification.Name.reachabilityChanged,
                                               object: reachability)
        do{
            try reachability?.startNotifier()
        } catch {
            debugPrint("Could not start reachability notifier")
        }
    }
    
    /**
     Function to remove observer for reachability change
     */
    func stopMonitoring(){
        reachability?.stopNotifier()
        NotificationCenter.default.removeObserver(self,
                                                  name: Notification.Name.reachabilityChanged,
                                                  object: reachability)
    }
    
    deinit {
//        stopMonitoring()
    }
}



